<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ListeController extends AbstractController
{
    /**
     * @Route("/liste", name="liste")
     */
    public function index()
    {
        $moviesList = file_get_contents( './data/movies.json');
        $json = json_decode($moviesList, true);

        return $this->render('liste/index.html.twig', [
            'Liste' => $json,
        ]);
    }
}
